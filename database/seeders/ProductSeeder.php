<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert(
            [
                "idService" => "39",
                "name" => "Pastillas de freno trasero",
                "status" => "1",
                "order" => "4",
                "ispricefrom" =>  "0",
                "type" =>  "1",
                "id_ws" =>  "CBS_REAR_BRAKE_PADS",
                "description" =>  "El servicio de sustitución de pastillas de freno traseras es realizada por especialistas de BMW Service utilizando siempre pastillas de freno Originales BMW de larga duración. Garantizando así el máximo confort de marcha y un magnífico comportamiento de los frenos, incluso en condiciones adversas.",
                "detail" =>  "En combinaci&oacute;n con los discos de freno Originales BMW, las pastillas de freno garantizan el &oacute;ptimo funcionamiento de los sistemas de control m&aacute;s importantes, como el Sistema Antibloqueo de los Frenos (ABS) y el Control Din&aacute;mico de Estabilidad (DSC), lo que garantiza la m&aacute;xima seguridad y rendimiento.Operaciones y recambios originales incluidos:- Comprobaci&oacute;n de mensajes en el cuadro de control.<br />- Comprobaci&oacute;n de testigos luminosos y de aviso.<br />- Sustituci&oacute;n de las pastillas de freno y sensor de desgaste.<br />- Comprobaci&oacute;n de la superficie y espesor de los discos de frenos.<br />- Comprobaci&oacute;n del funcionamiento del freno de mano.<br />- Restablecimiento el indicador de mantenimiento.",
                "disclaimer" =>  "Recambios Originales BMW, mano de obra e impuestos incluidos. Consulta&nbsp;a tu concesionario quien estar&aacute; encantado de ofrecerte&nbsp;un presupuesto personalizado.\n",
                "value" =>  "166.00",
            ]
        );
        DB::table('products')->insert(
            [
                "idService" => "40",
                "name" => "Servicio de aceite",
                "status" => "1",
                "order" => "1",
                "ispricefrom" =>  "0",
                "type" =>  "1",
                "id_ws" =>  "CBS_OIL_SERVICE",
                "description" =>  "El servicio de aceite es realizado por especialistas de BMW Service utilizando recambios Originales BMW y Aceite Genuino BMW que cumple la máxima cualificación de los aceites sintéticos modernos. Todo esto te asegura un óptimo rendimiento del motor de tu BMW.",
                "detail" =>  "Operaciones y recambios originales incluidos:- Comprobaci&oacute;n del funcionamiento del freno de mano.<br />-&nbsp;Comprobaci&oacute;n de mensajes en el cuadro de control.<br />- Comprobaci&oacute;n de testigos luminosos y de aviso.<br />- Sustituci&oacute;n del aceite de motor.<br />- Sustituci&oacute;n del filtro de aceite.<br />- Eliminaci&oacute;n respetuosa con el medio ambiente del aceite usado y el filtro desechado.<br />- Restablecimiento del indicador de servicio de mantenimiento ",
                "disclaimer" =>  "Aceite Genuino BMW, recambios Originales BMW, mano de obra e impuestos incluidos. Consulta&nbsp;a tu concesionario quien estar&aacute; encantado de ofrecerte un presupuesto personalizado.\n",
                "value" =>  "149.00",
            ]
        );
        DB::table('products')->insert(
            [
                "idService" => "73",
                "name" => "Servicio microfiltro mejorado",
                "status" => "1",
                "order" => "6",
                "ispricefrom" =>  "0",
                "type" =>  "5",
                "id_ws" =>  "CBS_MICROFILTER",
                "description" =>  "Con cada cambio de aceite se debe sustituir el microfiltro del sistema de climatización, permitiendo respirar mejor, especialmente a las personas alérgicas. Para este mantenimiento los especialistas de BMW Service utilizan recambios Originales BMW los cuales se adaptan perfectamente a tu vehículo",
                "detail" =>  "El filtro de aire Original BMW te asegura que el polvo y suciedad en el aire de admisión no llegue al motor, proporcionándole siempre la cantidad apropiada de aire limpio. Las principales ventajas de los filtros de aire Originales BMW son: \n \n- Están diseñados para las variantes específicas de cada motor \n- Capacidad de retención óptima<br /> \n- Papel de filtro impregnado con resinas especiales para una mayor resistencia<br /> \n- Óptimos niveles de separación de polvo de hasta 99%<br /> \n- Suministro óptimo de aire para un menor consumo de combustible \nLas bujías Originales BMW están fabricadas con materiales de alta calidad, resistentes a altas temperaturas, lo que garantiza una fiabilidad del 100% durante arranques en frio y aceleración. Otras características de las bujías Originales BMW de las cuales te podrás beneficiar son: \n- Alta protección a la corrosión<br /> \n- Larga vida útil y gran fiabilidad<br /> \n- Perfecta alineación del material para prevenir fugas y fallos de encendido<br /> \n- Diseñadas y probadas para tu BMW<br /> \n- 2 años de garantía \nOperaciones y recambios originales incluidos: \n-\tSustitución del filtro de aire<br /> \n-\tSustitución de las bujías de encendido",
                "disclaimer" =>  "Recambios Originales BMW, mano de obra e impuestos incluidos. Consulta a tu concesionario quien estará encantado de ofrecerte un presupuesto personalizado.",
                "value" =>  "75.00",
            ]
        );
        DB::table('products')->insert(
            [
                "idService" => "54",
                "name" => "Servicio filtro de aire",
                "status" => "1",
                "order" => "6",
                "ispricefrom" =>  "0",
                "type" =>  "5",
                "id_ws" =>  "CBS_AIR_FILTER",
                "description" =>  "Cada segundo o tercer cambio de aceite tu BMW puede necesitar la sustitución del filtro de aire para seguir proporcionándote un máximo rendimiento. Para este mantenimiento los especialistas de BMW Service utilizan recambios Originales BMW los cuales se adaptan perfectamente a tu vehículo.",
                "detail" =>  "El filtro de aire Original BMW te asegura que el polvo y suciedad en el aire de admisión no llegue al motor, proporcionándole siempre la cantidad apropiada de aire limpio. Las principales ventajas de los filtros de aire Originales BMW son: \n \n- Están diseñados para las variantes específicas de cada motor \n- Capacidad de retención óptima<br /> \n- Papel de filtro impregnado con resinas especiales para una mayor resistencia<br /> \n- Óptimos niveles de separación de polvo de hasta 99%<br /> \n- Suministro óptimo de aire para un menor consumo de combustible \nLas bujías Originales BMW están fabricadas con materiales de alta calidad, resistentes a altas temperaturas, lo que garantiza una fiabilidad del 100% durante arranques en frio y aceleración. Otras características de las bujías Originales BMW de las cuales te podrás beneficiar son: \n- Alta protección a la corrosión<br /> \n- Larga vida útil y gran fiabilidad<br /> \n- Perfecta alineación del material para prevenir fugas y fallos de encendido<br /> \n- Diseñadas y probadas para tu BMW<br /> \n- 2 años de garantía \nOperaciones y recambios originales incluidos: \n-\tSustitución del filtro de aire<br /> \n-\tSustitución de las bujías de encendido",
                "disclaimer" =>  "Recambios Originales BMW, recambios Originales BMW, mano de obra e impuestos incluidos. Consulta&nbsp;a tu concesionario quien estar&aacute; encantado de ofrecerte un presupuesto personalizado.\n",
                "value" =>  "70.00",
            ]
        );
        DB::table('products')->insert(
            [
                "idService" => "53",
                "name" => "Servicio Filtro de combustible",
                "status" => "1",
                "order" => "5",
                "ispricefrom" =>  "0",
                "type" =>  "1",
                "id_ws" =>  "CBS_FUEL_FILTER",
                "description" =>  "Cada segundo o tercer cambio de aceite tu BMW puede necesitar la sustitución del filtro de combustible para seguir proporcionándote un máximo rendimiento. Para este mantenimiento los especialistas de BMW Service utilizan recambios Originales BMW los cuales se adaptan perfectamente a tu vehículo.",
                "detail" =>  "El filtro de aire Original BMW te asegura que el polvo y suciedad en el aire de admisi&oacute;n no llegue al motor, proporcion&aacute;ndole siempre la cantidad apropiada de aire limpio.
                                Las principales ventajas de los filtros de aire Originales BMW son:- Dise&ntilde;ados para las variantes espec&iacute;ficas de cada motor<br />- Capacidad de retenci&oacute;n &oacute;ptima<br />-
                                Papel de filtro impregnado con resinas especiales para una mayor resistencia<br />- &Oacute;ptimos niveles de separaci&oacute;n de polvo de hasta 99%<br />- Suministro &oacute;ptimo de aire para un menor consumo de
                                combustibleEl filtro de combustible Original BMW remueve todas las part&iacute;culas de suciedad que se encuentran en el combustible. En el caso de los motores a diesel, el agua contenida en el gas&oacute;leo tambi&eacute;n es
                                filtrada. Otras caracter&iacute;sticas de los filtros de combustible Originales BMW de las cuales te podr&aacute;s beneficiar son:- Materiales de filtrado de alta calidad<br />- Protecci&oacute;n m&aacute;xima frente a la
                                corrosi&oacute;n<br />- Dise&ntilde;ados para las altas presiones de los motores BMW<br />- Contenci&oacute;n segura en caso de accidenteOperaciones y recambios originales incluidos:<span style=line-height:1.6em>-
                                Sustituci&oacute;n del filtro de aire</span><br /><span style=line-height:1.6em>- Sustituci&oacute;n del filtro de combustible</span>",
                "disclaimer" =>  "Recambios Originales BMW, mano de obra e impuestos incluidos. Consulta&nbsp;a tu concesionario quien estar&aacute; encantado de ofrecerte&nbsp;un presupuesto personalizado.\n",
                "value" =>  "75.00",
            ]
        );
    }
}

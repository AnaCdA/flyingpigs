<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('idService');
            $table->string('name');
            $table->integer('status');
            $table->integer('order');
            $table->float('isPriceFrom');
            $table->integer('type');
            $table->string('id_ws');
            $table->longText('description');
            $table->longText('detail');
            $table->longText('disclaimer');
            $table->float('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

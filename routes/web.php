<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OwnerController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main/master');
});

Route::get('/propietarios', [OwnerController::class, 'index'])->name('propietarios');
Route::post('/propietarios', [OwnerController::class, 'showOwnerProducts', '$variable']);
Route::get('/fetch/products', [OwnerController::class, 'fetchProducts']);



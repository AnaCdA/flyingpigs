## Prueba técnica para FlyingPigs 

Este proyecto son los archivos de la prueba técnica para Flying Pigs. <br><br>Ha sido desarrollado con Laravel y tiene instalados React y
graphql. <br>El servidor local que he utilizado es Xampp, aunque no debería suponer ningún problema montarlo en otro.<br>En el archivo
httpd-vhosts, al ser un proyecto de Laravel, el servidor tiene que apuntar a la siguiente ruta: 'C:\xampp\htdocs\flyingpigs\public' 
y el serverAdmin es 'flyingpigs.test'. Una vez montado hay que lanzar los siguientes comandos en la consola:

- composer install 
- npm install
- php artisan migrate
- php artisan db:seed --class=ProductSeeder


Si todo va bien la vista se encuentra en http://flyingpigs.test/propietarios . Si hay algún problema con la instalación del proyecto
no dudéis en contactarme y ayudaré en lo que pueda o podemos verlo en mi equipo.

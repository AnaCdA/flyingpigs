@extends('main.master')

@section('content')
    <div class="container border-top" id="cardContainer">
        <div class="row justify-content-sm-center" id="registrationCardContainer">
            <div class="col" id="carPhoto">
                <img class="img-fluid" src="{{ asset('images/F34.png') }}"/>
            </div>
            <div class="col mt-2">
                <h2>BMW SERIE 3</h2>
                <div class="row">
                    <div class="col ps-0">
                         <div class="card-body p-1 m-1" id="registrationCard">
                             <!--<h6 class="card-subtitle mb-2 text-muted ms-0 fw-light">MATRÍCULA</h6>
                             <p class="card-text text-bold fs-5 col mb-0">1234-XYZ </p> -->
                             <h6 class="card-subtitle mb-2 text-muted ms-0 fw-light fs-6">MATRÍCULA</h6>
                             <div class="row"><p class="card-text col fs-5">F1234-XYZ</p> <a class="col fs-6 col">Cambiar</a></div>
                         </div>
                    </div>
                    <div class="col ps-0">
                        <div class="card-body p-1 m-1">
                            <h6 class="card-subtitle mb-2 text-muted ms-0 fw-light fs-6">MODELO</h6>
                            <p class="card-text fs-5">F20</p>
                        </div>
                    </div>
                    <div class="row pb-0 mb-5" id="mark">
                        <img class="co p-0" src="{{ asset('images/marcador.png') }}"/></im><p class="col text-sm mb-0">Girona, <strong>Olivia Motor</strong></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row  justify-content-sm-center">
                <div class="col">
                    <div class="card-body p-1 m-1">
                        <h6 class="card-subtitle m-2 text-muted ms-0 fw-light fs-6">PAQUETE CONTRATADO</h6>
                        <p class="card-text">BMW Service inclusive</p>
                    </div>
                </div>
                <div class="col">
                    <div class="card-body p-1 m-1">
                        <h6 class="card-subtitle m-2 text-muted ms-0 fw-light fs-6">DURACIÓN</h6>
                        <p class="card-text">3 años / 60.000km.</p>
                    </div>
                </div>
                <div class="col">
                    <div class="card-body p-1 m-1">
                        <h6 class="card-subtitle m-2 text-muted ms-0 fw-light fs-6">FECHA DE INICIO</h6>
                        <p class="card-text">01 enero 2020</p>
                    </div>
                </div>
        </div>
    </div>
    <div id="rectangles" style="margin-bottom: 100px;">
        <div class="row">
            <div class="col fs-3 fw-bold h-75" id="whiteRectangle">
                <p class="text-sm-center">BMW Service Inclusive</p>
            </div>
            <div class="col fs-3 fw-bold text-sm-center" id="blackRectangle">
                <p class="textcenter">BMW Precios Cerrados</p>
            </div>
        </div>
    </div>
    <div class="container-sm mb-5">
        <div class="row">
            <div class="col-8">
               <div class="container border-bottom">
                   <div class="row">
                       <div class="col border-bottom border-primary border-2">
                           <i class="d-block d-md-none">
                               <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-dash-square" viewBox="0 0 16 16">
                                   <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                   <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"/>
                               </svg>
                           </i>
                           <strong class="d-none d-md-inline fs-4">Servicios</strong>
                       </div>
                       <div class="col">
                           <i class="d-block d-md-none">
                               <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-dash-square" viewBox="0 0 16 16">
                                   <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                   <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"/>
                               </svg>
                           </i>
                           <strong class="d-none d-md-inline fs-5" style="color: lightgrey">Tabla de mantenimiento</strong>
                       </div>
                       <div class="col text-end">
                           <i class="d-block d-md-none">
                               <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-dash-square" viewBox="0 0 16 16">
                                   <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                                   <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"/>
                               </svg>
                           </i>
                           <strong class="d-none d-md-inline fs-6" style="color: dimgrey;">Filtros
                               <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-funnel" viewBox="0 0 16 16">
                                   <path d="M1.5 1.5A.5.5 0 0 1 2 1h12a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.128.334L10 8.692V13.5a.5.5 0 0 1-.342.474l-3 1A.5.5 0 0 1 6 14.5V8.692L1.628 3.834A.5.5 0 0 1 1.5 3.5v-2zm1 .5v1.308l4.372 4.858A.5.5 0 0 1 7 8.5v5.306l2-.666V8.5a.5.5 0 0 1 .128-.334L13.5 3.308V2h-11z"/>
                               </svg>
                           </strong>
                       </div>
                   </div>
               </div>
               <div id="main" style="display: none;"></div>
                <div id="productsContainer">
                    @include('layouts.cardsLayout')
                </div>
                <div class="mx-auto"  id="button">
                    <button class="m-1" type="button" id="loadButton">Continuar</button>
                    <input type="hidden" id="currentPage" value="{{ $products->currentPage() }}">
                </div>
            </div>
            <div class="col-4" id="selectedServiceContainer0">
                <div class="container pt-0" id="selectedServiceContainer01">
                    <h4 class="text-center mb-3">Servicios seleccionados</h4>
                </div>
            </div>
        </div>
    </div>
@endsection

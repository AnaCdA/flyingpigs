@foreach($products as $product)
    <div class="border mt-5 mb-4 articleCard" id="articleCard{{$product->id}}">
        <div class="row">
            <div class="container pt-3">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="container">
                            <div class="row">
                                <div class="col-1">
                                    <input
                                        type="checkbox"
                                        class="checkBox productCheckBox{{$product->id}}"
                                        id="{{$product->id}}"
                                        name="{{ $product->name }}"
                                        price="{{$product->value}}"
                                    />
                                </div>
                                <div class="col">
                                    {{$product->name}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm">
                                    <p class="text-muted text-decoration-line-through">{{$product->value + 20}}&euro;</p>
                                    <div><p class="fw-bold">{{$product->value}}&euro;</p></div>
                                </div>
                                <div class="col-sm">
                                    <p>
                                        <a
                                            href="#"
                                            class="text-decoration-none moreDetails"
                                            moreDetails-id="{{ $product->id }}"
                                            id="{{$product->id}}"
                                            name="{{ $product->name }}"
                                            price="{{$product->value}}"
                                            description="{{$product->description}}"
                                        >
                                            Ver detalle
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-lg" viewBox="0 0 16 16">
                                                <path fill-rule="evenodd" d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"></path>
                                            </svg>
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blueCard ocultar border mt-2 mb-4 blueArticleCard0" id="blueCard{{$product->id}}">
        <div class="row">
            <div class="container pt-3">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="container">
                            <div class="row">
                                <div class="col-1" id="">
                                    <input type="checkbox" class="checkbox"/>
                                </div>
                                <div class="col">
                                    {{$product->name}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm">
                                    <p class="text-muted text-decoration-line-through">{{ $product->value + 20}}&#8364</p>
                                    <div><p class="fw-bold">{{ $product->value }}&#8364</p></div>
                                </div>
                                <div class="col-sm">
                                    <p>
                                        <a
                                            href="#"
                                            class="text-decoration-none closeDetails"
                                            closeDetails-id="{{$product->id}}"
                                        >
                                            Ocultar detalle
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x" viewBox="0 0 16 16">
                                                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                                            </svg>
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row border-top border-white" id="blueArticleCard1">
                        <div class="container">
                            <p class="p-4 mt-4" id="detailsText">
                               {{$product->description}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach




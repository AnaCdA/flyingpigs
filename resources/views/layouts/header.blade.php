<header class="p-3 mx-5">
    <div class="container">
        <div class="d-flex flex-wrap align-items-center justify-content-center">
            <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                <li><a href="#" class="nav-link px-2 link-dark">Modelos</a></li>
                <li><a href="#" class="nav-link px-2 link-dark">Buscar & Comprar</a></li>
                <li><a href="#" class="nav-link px-2 link-dark">Movilidad Electrica</a></li>
                <li><a href="#" class="nav-link px-2 link-dark">Propietarios</a></li>
                <li><a href="#" class="nav-link px-2 link-dark">Mundo BMW</a></li>
            </ul>
            <div class="iconsBar mb-2 me-2">
                <ul class="nav col-12 my-2  my-md-0 ">
                    <li>
                        <a href="#" class="nav-link text-secondary">
                            <img src="{{ asset('images/search.png') }}">
                        </a>
                    </li>
                    <li>
                        <a href="#" class="nav-link text-white">
                            <img src="{{ asset('images/user.png') }}">
                        </a>
                    </li>
                    <li>
                        <a href="#" class="nav-link text-white">
                            <img src="{{ asset('images/marcador.png') }}">
                        </a>
                    </li>
                </ul>
            </div>
            <div class="d-flex justify-content-end">
                <img src="{{ asset('images/bmwIcon.svg') }}">
            </div>
        </div>
    </div>
</header>

$(document).on('click', '#loadButton', function () {
    var page = parseInt($('#currentPage').val()) + 1;

    $.ajax({
        type : 'GET',
        url : '/fetch/products?page='+page,
        dataTYpe : 'html',
        encode : true,
        success : function(response){
            $('#currentPage').val(page);
            $('#productsContainer').append(response)
        }
    });
})

$(document).on('change', '.checkBox', function () {
    if ($(this).is(':checked')){
        $('#selectedServiceContainer01').append(
            '<div class="container pe-0 selectedServiceContainer1" id="selectedProduct' + $(this).attr("id") + '">\n' +
            '    <div>\n' +
            '        <div class="row selectedService">\n' +
            '            <div class="col-1"></div>\n' +
            '            <div class="col mb-1">\n' +
            '                <p class="m-0">' + $(this).attr("name") + '</p>\n' +
            '            </div>\n' +
            '            <div class="col-1 closeProductIcon" attr-id="' + $(this).attr("id") + '">\n' +
            '                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x"\n' +
            '                     viewBox="0 0 16 16">\n' +
            '                    <path\n' +
            '                        d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>\n' +
            '                </svg>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '        <div class="row selectedService">\n' +
            '            <div class="col-1"></div>\n' +
            '            <div class="col">\n' +
            '                <p>' + $(this).attr("price") + '&#8364</p>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '    </div>\n' +
            '</div>'
        )
    }else {
        $('#selectedProduct' + $(this).attr("id")).remove();
    }
})

$(document).on('click', '.closeProductIcon', function () {
    $('#selectedProduct' + $(this).attr("attr-id")).remove();
    $('.productCheckBox' + $(this).attr("attr-id")).prop( "checked", false );
})

$(document).on('click', '.moreDetails', function (e) {
    e.preventDefault()
    console.log('rr')
    var id = $(this).attr("moreDetails-id")



    if ($('#blueCard' + id).hasClass('ocultar')) {
        $('#blueCard' + id).removeClass('ocultar')
        $('#articleCard' + id).addClass('ocultar')
    } else {
        $('#blueCard' + id).addClass('ocultar')
    }
})

$(document).on('click', '.closeDetails', function (e) {
    e.preventDefault()
    console.log('rr')
    var id = $(this).attr("closeDetails-id")

    $('#blueCard' + id).addClass('ocultar')
    $('#articleCard' + id).removeClass('ocultar')

})



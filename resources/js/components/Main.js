import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { ApolloClient, ApolloProvider, InMemoryCache, HttpLink, gql} from "@apollo/client";

const client = new ApolloClient({
    link: new HttpLink({
        uri: 'http://flyingpigs.test/graphql',
    }),
    cache: new InMemoryCache()
});

/*ReactDOM.render(
    < ApolloProvider client={client}>
        <App />
    </ApolloProvider>,
    document.getElementById('main'),
);*/

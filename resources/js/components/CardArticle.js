import React, {useEffect, useState} from 'react'
import {gql, useQuery} from "@apollo/client";
import '../../../public/css/app.css'

const ALL_PRODUCTS =  gql`
    query products {
        products {
            id
            name
            value
        }
    }
`
function CardArticle(){
    const {data, error, loading} = useQuery(ALL_PRODUCTS)

    if (!loading) {
        const products = data.products
        return(
            <div>
                {products.map((product, i) => {
                    return (
                        <div className="border mt-5 mb-4 articleCard" key={i}>
                            <div className="row">
                                <div className="container pt-3">
                                    <div className="row">
                                        <div className="col-sm-8">
                                            <div className="container">
                                                <div className="row">
                                                    <div className="col-1">
                                                        <input type="checkbox"/>
                                                    </div>
                                                    <div className="col">
                                                        {product.name}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-4">
                                            <div className="container">
                                                <div className="row">
                                                    <div className="col-sm">
                                                        <p className="text-muted text-decoration-line-through">{product.value +20}&euro;</p>
                                                        <div><p className="fw-bold">{product.value}&euro;</p></div>
                                                    </div>
                                                    <div className="col-sm">
                                                        <p>
                                                            <a href="#" className="text-decoration-none">
                                                                Ver detalle
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                                     fill="currentColor" className="bi bi-plus-lg"
                                                                     viewBox="0 0 16 16">
                                                                    <path fillRule="evenodd"
                                                                          d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"></path>
                                                                </svg>
                                                            </a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>
        )
    }
    return(<div>Loading...</div>)
}

export default CardArticle

<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class OwnerController extends Controller
{
    public function index()
    {
        return view('layouts.owners', [
            'products' => Product::paginate(3),
        ]);
    }

    function fetchProducts(Request $request)
    {
        if($request->ajax())
        {
            $products = Product::paginate(3);
            return view('layouts.cardsLayout', ['products' => $products])->with('currentPage', $products->currentPage()) ;
        }
    }


}
